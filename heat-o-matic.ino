/***
 * Heat-O-Matic
 *
 * License: Eclipse Public License (EPL)
 */

#include <OneWire.h>
#include <DallasTemperature.h>

#include <SED1531.h>
#include <proportional_font.h>
#include <fixed_font.h>

#include <PID_v1.h>

 #include <EEPROMex.h>

#define RELAY_PIN A0

// ID of the settings block
#define CONFIG_VERSION "hm1"

// Tell it where to store your config data in EEPROM
#define memoryBase 350
int configAdress=0;

const int maxAllowedWrites = 80;

// Example settings structure
struct StoreStruct {
    char version[4];   // This is for mere detection if they are your settings
    uint16_t setpoint, boost, Kp, Ki, Kd;
    int16_t calibration;
} storage;


#define DS18B20_PIN A1
#define LCD_BACKLIGHT_PIN 10

#define DS_TEMP_INTERVAL 2000

OneWire ds(DS18B20_PIN);
DallasTemperature ds_sensors(&ds);

#define THERMRISTOR_PIN A6
#define THERMRISTOR_INTERVAL 5000

SED1531 glcd;

bool forceRefresh;

uint32_t ds_measure_time, button_read_time, longButtonTime, thermristor_time = 0;

double thermristorTemp = 0;



//Define Variables we'll be connecting to
double Setpoint, Input, Output;

//Specify the links and initial tuning parameters
double Kp=800, Ki=0, Kd=50;
PID myPID(&Input, &Output, &Setpoint, Kp, Ki, Kd, DIRECT);

static const int WindowSize = 5000;
unsigned long windowStartTime;
bool relayEnable;

int32_t button_avg;
byte buttonState = 0, lastButtonState = 0, sendButtonState = 0;

bool heatingEnabled = false;
bool heatingOn = false;

#define BTN_1 1
#define BTN_2 2
#define BTN_3 4
#define BTN_4 8

enum DISPLAYS
{
	DISP_STATUS,
	DISP_MENU,
	DISP_MENU_EDIT
} displayState;

double calibration = 0;
uint16_t boost = 300;

void setup()
{
	pinMode(RELAY_PIN, OUTPUT);
	digitalWrite(RELAY_PIN, LOW);
	
	heatingEnabled = false;

	// Configure backlight pin, set low on boot
	pinMode(LCD_BACKLIGHT_PIN, OUTPUT);
	digitalWrite(LCD_BACKLIGHT_PIN, 0);

	glcd.init();
	glcd.clearDisplay();

	forceRefresh = true;
	digitalWrite(LCD_BACKLIGHT_PIN, HIGH);
	//analogWrite(LCD_BACKLIGHT_PIN, 255);

	ds_sensors.begin();

	ds_measure_time = 0;
	button_read_time = 50 + ds_measure_time;
	windowStartTime = 10 + ds_measure_time;

	Setpoint = 50;
	EEPROM.setMaxAllowedWrites(maxAllowedWrites);
	EEPROM.setMemPool(memoryBase, EEPROMSizeUno); //Set memorypool base to 32, assume Arduino Uno board
  	configAdress = EEPROM.getAddress(sizeof(StoreStruct)); // Size of config object 
  	
  	if (!loadConfig())
  	{
		saveConfig();

		glcd.clearDisplay();
		glcd.setFont(&proportional_font);
		glcd.setCursor(0,0);
		glcd.print("Reset config");

		if (!loadConfig())
			glcd.print(" Fail");
		else
			glcd.print(" OK");
		delay(5000);
  	}

  	myPID.SetSampleTime(5000);

  	myPID.SetTunings(Kp, Ki, Kd);

	Input = 20;

	//tell the PID to range between 0 and the full window size
	myPID.SetOutputLimits(-1*(double)boost, WindowSize);

	//turn the PID on
	myPID.SetMode(AUTOMATIC);

	relayEnable = false;

	button_avg = 1024;

	displayState = DISP_STATUS;
}


bool loadConfig() {
	EEPROM.readBlock(configAdress, storage);
	if (storage.version[0] == 'h' &&storage.version[1] == 'm' && storage.version[2] == '1' && storage.version[3] == 0)
	{
		Setpoint = ((double)storage.setpoint) / 10;
		boost = storage.boost;
		calibration = ((double)storage.calibration) / 100;

		Kp = (double) storage.Kp;
		Ki = (double) storage.Ki;
		Kd = (double) storage.Kd;

		return true;
	}

	return false;
}

void saveConfig() {
	storage.version[0] = 'h';
	storage.version[1] = 'm';
	storage.version[2] = '1';
	storage.version[3] = 0;
	storage.setpoint = (uint16_t) (Setpoint * 10);
	storage.boost = (uint16_t) boost;
	storage.calibration = (int16_t) (calibration * 100);
	storage.Kp = (uint16_t) Kp;
	storage.Ki = (uint16_t) Ki;
	storage.Kd = (uint16_t) Kd;

	EEPROM.writeBlock(configAdress, storage);
}

void loop()
{
	draw();

	uint16_t buttons = analogRead(A7);
	if (abs(button_avg - buttons) > 20)
	{
		button_avg = buttons;
		button_read_time = millis() - 20;
	}
	else
	{
		button_avg = (button_avg * 7 + buttons) / 8;
	}

	buttonState = 0;

	if (millis() - button_read_time >= 50)
	{
		byte current_button = 0;

		button_read_time = millis();
		current_button = readButtons(button_avg);
		
		if (abs(button_avg - buttons) < 20 && current_button == lastButtonState)
		{
			if (sendButtonState != lastButtonState)
			{
				buttonState = current_button;
				sendButtonState = current_button;

				handleButtons();

				longButtonTime = millis();
			}
			else if (longButtonTime > 0 && millis() - longButtonTime > 1000)
			{
				buttonState = sendButtonState;
				handleLongButtons();
				longButtonTime = 0;
			}
		}
		else
		{
			longButtonTime = 0;
		}

		lastButtonState = current_button;
		
		
		//glcd.setCursor(0, 24);
		//glcd.print(buttons);
		//glcd.print(" ");
		//glcd.print(button_avg);
		//glcd.print(" ");
		//glcd.print(current_button);
		//glcd.print("  ");
		
	}

	else if (ds_measure_time == 0 || millis() - ds_measure_time >= DS_TEMP_INTERVAL)
	{
		ds_measure_time = millis();

		float temp;

		if (ds_sensors.requestTemperaturesByIndex(0) && (temp = ds_sensors.getTempCByIndex(0)) != DEVICE_DISCONNECTED_C)
		{
			Input = temp + calibration;
		}
		else
		{
			glcd.setCursor(0, 40);
			glcd.print(F("None")); 
		}

	}

	else if (thermristor_time == 0 || millis() - thermristor_time >= THERMRISTOR_INTERVAL)
	{
		thermristor_time = millis();
		uint16_t RawADC = analogRead(THERMRISTOR_PIN);

		thermristorTemp = log(10000.0*((1024.0/RawADC-1))); 
		//         =log(10000.0/(1024.0/RawADC-1)) // for pull-up configuration
		thermristorTemp = 1 / (0.001129148 + (0.000234125 + (0.0000000876741 * thermristorTemp * thermristorTemp ))* thermristorTemp );
		thermristorTemp = thermristorTemp - 273.15;            // Convert Kelvin to Celcius
		//Temp = (Temp * 9.0)/ 5.0 + 32.0; // Convert Celcius to Fahrenheit
		
	}

	if (myPID.Compute())
	{
		Output += boost;
		if (Output > WindowSize)
		{
			Output = WindowSize;
		}
		else if (Output < 0)
		{
			Output = 0;
		}

		windowStartTime = millis();
		relayEnable = true;
	}	

	if (heatingEnabled && relayEnable && millis() - windowStartTime < Output) 
	{
		
		if (!heatingOn)
		{
			digitalWrite(RELAY_PIN, HIGH);
			glcd.setIndicator(GLCD_INDICATOR_4, 1);
			heatingOn = true;
		}
	}
	else 
	{
		relayEnable = false;
		
		digitalWrite(RELAY_PIN, LOW);
		
		if (heatingOn)
		{
			glcd.setIndicator(GLCD_INDICATOR_4, 0);

			heatingOn = false;
		}
	}
}
 
void handleButtons()
{
	switch (displayState)
	{
		case DISP_STATUS:
			handleStatusButtons();
			break;

		case DISP_MENU:
			handleButtonsMenu();
			break;

		case DISP_MENU_EDIT:
			handleButtonsEditMenu();
			break;
	}
}

void handleLongButtons()
{
	switch (displayState)
	{
		case DISP_STATUS:
			if (buttonState == BTN_2)
			{
				heatingEnabled = !heatingEnabled;
				forceRefresh = true;
			}
			break;

		case DISP_MENU:
			//handleButtonsMenu();
			break;

		case DISP_MENU_EDIT:
			handleButtonsEditMenuLong();
			break;

	}
}

void draw()
{

	switch (displayState)
	{
		case DISP_STATUS:
			drawStatus();
			break;

		case DISP_MENU:
			if (forceRefresh)
			{
				drawMenu();
			}
			break;

		case DISP_MENU_EDIT:
			if (forceRefresh)
			{
				drawEditScreen();
			}
			break;

		default:
			displayState = DISP_STATUS;
			break;
	}

	forceRefresh = false;
}

double drawnTemp = 0;

void handleStatusButtons()
{
	if (buttonState == BTN_1)
	{
		displayState = DISP_MENU;
		forceRefresh = true;
	}
	else
	{
		drawnTemp = -1000;
	}
}

uint32_t lastStatusDraw = 0;
void drawStatus()
{
	if (forceRefresh)
	{
		glcd.setFont(&proportional_font);
		
		glcd.clearDisplay();

		glcd.setCursor(23, 0);
		glcd.print(F("HEAT-O-MATIC"));

		glcd.setIndicator(GLCD_INDICATOR_0, heatingEnabled?1:0);

		glcd.setCursor(0, 16);
		glcd.print(F("Set."));

		glcd.setCursor(30, 16);
		glcd.print(F("Cur."));

		glcd.setCursor(60, 16);
		glcd.print(F("Pwr"));

		glcd.drawFrame(94, 0, 6, 48, 1, 1);
	}

	if (forceRefresh || drawnTemp != Output + Input || millis() - lastStatusDraw >= 2000)
	{
		lastStatusDraw = millis();
		drawnTemp = Output + Input;

		glcd.drawBox(0, 24, 94, 8, 0);

		glcd.setCursor(0, 24);
		glcd.print(Setpoint, 1); 

		glcd.setCursor(30, 24);
		glcd.print(Input, 1); 

		glcd.setCursor(60, 24);
		glcd.print((uint16_t)Output);

		uint8_t prc = 0;

		if (heatingEnabled)
			prc = ((float) Output / WindowSize) * 44;

		glcd.drawBox(96, 46 - prc, 2, prc, 1);
		glcd.drawBox(96, 1, 2, 45 - prc, 0);

		glcd.setCursor(0, 40);
		glcd.print(thermristorTemp, 1);

		glcd.setCursor(40, 40);
		
	}
}

uint8_t menuItem = 1;
uint8_t menuOffset = 0;

#define DISPLAY_MENU_ITEMS 6
#define MENU_ITEMS 7

void handleButtonsMenu()
{
	if (buttonState == BTN_4)
	{
		menuItem++;
		forceRefresh = true;
	}
	if (buttonState == BTN_3)
	{
		menuItem--;
		forceRefresh = true;
	}

	if (buttonState == BTN_2)
	{
		if (menuItem == 7)
		{
			saveConfig();
			glcd.setIndicator(GLCD_INDICATOR_5, 1);
		}
		else
		{
			displayState = DISP_MENU_EDIT;
			forceRefresh = true;
		}
	}

	if (buttonState == BTN_1)
	{
		displayState = DISP_STATUS;
		forceRefresh = true;
	}

	if (menuItem < 1)
	{
		menuItem = MENU_ITEMS;
		menuOffset = MENU_ITEMS - DISPLAY_MENU_ITEMS;
	}

	if (menuItem > MENU_ITEMS)
	{
		menuItem = 1;
		menuOffset = 0;
	}

	if (menuOffset > 0 && menuItem - menuOffset == 1)
	{
		menuOffset--;
	}

	if (menuOffset < MENU_ITEMS - DISPLAY_MENU_ITEMS)
	{
		if (menuItem - menuOffset >= DISPLAY_MENU_ITEMS)
		{
			menuOffset++;
		}
	}
}

void drawMenu()
{
	uint8_t itemNr = 1;

	glcd.setFont(&fixed_font);

	/*
	glcd.setCursor(0,0);
	glcd.print(menuItem);
	glcd.print(":");
	glcd.print(menuOffset);
	glcd.print(" ");
	*/

	if (drawMenuItem(itemNr++, F("Setpoint"), 8))
		glcd.print(Setpoint, 1);

	if (drawMenuItem(itemNr++, F("Calibr."), 7))
		glcd.print(calibration);

	if (drawMenuItem(itemNr++, F("Boost"), 5))
		glcd.print(boost);

	if (drawMenuItem(itemNr++, F("Kp"), 2))
		glcd.print((uint16_t) Kp);

	if (drawMenuItem(itemNr++, F("Ki"), 2))
		glcd.print((uint16_t) Ki);
	
	if (drawMenuItem(itemNr++, F("Kd"), 2))
		glcd.print((uint16_t) Kd);

	drawMenuItem(itemNr++, F("Save config"), 11);

}

bool drawMenuItem(uint8_t nr, const __FlashStringHelper * txt, uint8_t txt_len)
{
	int8_t position = nr - menuOffset - 1;

	if (position < 0 || position >= DISPLAY_MENU_ITEMS)
		return false;

	bool current = nr == menuItem;

	uint8_t y = position * 8;

	glcd.setCursor(0, y);
	if (current)
	{
		glcd.print(">");
		glcd.drawBox(0, y - 1, 4, 9, 2);
	}
	else
	{
		glcd.print(" ");
	}
	glcd.print(txt);

	glcd.drawBox(txt_len * 6 + 6, y, 94 - (txt_len * 6), 8, 0);
	
	glcd.setCursor(60, y);

	return true;
}

int16_t editValue = 0;
uint8_t editDecimals = 0;
int16_t editMinValue = 0;
int16_t editMaxValue = 0;
uint8_t editPosition = 0;
uint8_t editMaxPosition = 0;
uint16_t editStepAmount = 1;

void handleButtonsEditMenu()
{
	if (buttonState == BTN_1)
	{
		editPosition++;
		editStepAmount *= 10;
		if (editPosition > editMaxPosition)
		{
			editPosition = 1;
			editStepAmount = 1;
		}

		drawEditMenuValue();
	}

	else if (buttonState == BTN_2)
	{
		editPosition--;
		editStepAmount /= 10;
		if (editPosition == 0)
		{
			editPosition = editMaxPosition;
			editStepAmount = pow(10, (editMaxPosition - 1)) + 1;
		}

		drawEditMenuValue();
	}

	else if (buttonState == BTN_3)
	{
		editValue -= editStepAmount;

		if (editValue < editMinValue)
			editValue = editMinValue;

		drawEditMenuValue();
	}
	else if (buttonState == BTN_4)
	{
		editValue += editStepAmount;

		if (editValue > editMaxValue)
			editValue = editMaxValue;

		drawEditMenuValue();
	}
}

void handleButtonsEditMenuLong()
{
	if (buttonState == BTN_1)
	{
		displayState = DISP_MENU;
		forceRefresh = true;
	}

	if (buttonState == BTN_2)
	{
		displayState = DISP_MENU;
		forceRefresh = true;

		switch (menuItem)
		{
			case 1:
				Setpoint = ((double) editValue) / 10;
				break;

			case 2:
				calibration = ((double) editValue) / 100;
				break;
						
			case 3:
				boost = editValue;
				myPID.SetOutputLimits(-1*(double)boost, WindowSize);
				break;
						
			case 4:
				Kp = ((double) editValue);
				myPID.SetTunings(Kp, Ki, Kd);
				break;
						
			case 5:
				Ki = ((double) editValue);
				myPID.SetTunings(Kp, Ki, Kd);
				break;
						
			case 6:
				Kd = ((double) editValue);
				myPID.SetTunings(Kp, Ki, Kd);
				break;
			default:
				return;
		}
	}
}

void drawEditScreen()
{
	uint8_t itemNr = 1;

	glcd.setFont(&fixed_font);

	drawEditMenuItem(itemNr++, F("Setpoint"), (int16_t) (Setpoint * 10), 1, 0, 1000);
		
	drawEditMenuItem(itemNr++, F("Calibr."), (int16_t) (calibration * 100), 2, -400, 400);
	
	drawEditMenuItem(itemNr++, F("Boost"), (int16_t) boost, 0, 0, 1000);

	drawEditMenuItem(itemNr++, F("Kp"), (int16_t) Kp, 0, 0, 2000);

	drawEditMenuItem(itemNr++, F("Ki"), (int16_t) Ki, 0, 0, 2000);
	
	drawEditMenuItem(itemNr++, F("Kd"), (int16_t) Kd, 0, 0, 2000);

}

void drawEditMenuItem(uint8_t nr, const __FlashStringHelper * txt, int16_t value, uint8_t decimals, int16_t min_value, int16_t max_value)
{
	if (menuItem != nr)
		return;

	editDecimals = decimals;
	editMaxValue = max_value;
	editMinValue = min_value;

	editValue = min(max_value, max(min_value, value));

	editStepAmount = 1;

	// Determine max nr of positions
	int16_t t = max(abs(min_value), abs(max_value));
	editMaxPosition = 1;
	while (t >= 10)
	{
		editMaxPosition++;
		t /= 10;
		editStepAmount *= 10;
	}

	editPosition = editMaxPosition;

	glcd.clearDisplay();

	glcd.setCursor(30, 8);
	glcd.print(txt);
	
	drawEditMenuValue();
}

void drawEditMenuValue()
{
	uint16_t s = 1;
	int16_t v = editValue;

	uint8_t signDone = 0;
	uint8_t x = 20 + 7*6;

	if (editDecimals)
		x += 6;

	/*
	glcd.setCursor(0, 0);
	glcd.print(v);
	glcd.print(" ");
	glcd.print(editStepAmount);
	glcd.print(" ");
	glcd.print(editPosition);
	glcd.print(" ");
	glcd.print(editMaxPosition);
	glcd.print(" ");
	*/

	for (uint8_t pos = 0; pos < 7; pos++)
	{
		glcd.setCursor(x, 20);
		x -= 6;
		
		if (pos > editDecimals && (v == 0 || pos > editMaxPosition))
		{
			signDone++;
			if (signDone == 2 && editValue < 0)
			{
				glcd.print("-");
			}
			else
			{
				glcd.print(" ");
			}
		}
		else
		{
			uint8_t d = (uint8_t) abs(v % 10);
			
			if (editDecimals > 0 && pos == editDecimals)
			{
				glcd.print(".");
				glcd.setCursor(x, 20);
				x -= 6;
			}

			glcd.print(d);
			v /= 10;
		}
	}

	if (signDone == 1 && editValue < 0)
	{
		glcd.setCursor(x, 20);
		glcd.print("-");
	}

	x = 20 + (6 * (8 - editPosition)) + ((editPosition <= editDecimals)? 6:0);
	
	glcd.drawBox(0, 28, 100, 2, 0);
	glcd.drawBox(x, 28, 6, 2, 1);
}

byte readButtons(uint16_t v)
{
	

	if (v > 500)
	{
		return 0;
	}
	else if (v > 320 )
	{
		return BTN_1;
	}
	else if (v > 250) 
	{
		return BTN_2;
	}
	else if (v > 195)
	{
		return BTN_1 + BTN_2;
	}
	else if (v > 160)
	{
		return BTN_3;
	}
	else if (v > 135)
	{
		return BTN_1 + BTN_3;
	}
	else if (v > 115)
	{
		return BTN_2 + BTN_3;
	}
	else if (v > 100)
	{
		return BTN_4;
	}
	else if (v > 85)
	{
		return BTN_1 + BTN_4;
	}
	else if (v > 73)
	{
		return BTN_2 + BTN_4;
	}
	else if (v > 61)
	{
		return BTN_3 + BTN_4;
	}
	else if (v < 55)
	{
		return BTN_1 + BTN_2 + BTN_3 + BTN_4;
	}

	return 0;
}